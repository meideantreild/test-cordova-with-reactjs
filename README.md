how to build hybrid app mobile using cordova with reactjs

1. Add project reactjs https://reactjs.org/docs/create-a-new-react-app.html
2. Install global cordova https://cordova.apache.org/#getstarted
3. Open terminal project 'npm run eject' *without '' , it will create folder config
4. Open config/path.js. Search appBuild, change line into appBuild: resolveApp('www')
5. Open package.json and add "homepage": "./", whatever u want take this code
6. Add cordova into project $ cordova create hello com.example.hello HelloWorld https://cordova.apache.org/docs/en/latest/guide/cli/index.html
7. After insert cordova, it will create folder hello. Open folder 'hello' and move file config.xml into root project
8. Config src/index.js
    ```
    import React from 'react';
    import ReactDOM from 'react-dom';
    import './index.css';
    import App from './App';
    import * as serviceWorker from './serviceWorker';
    
    const startApp = () => {
        ReactDOM.render(<App />, document.getElementById('root'));
        serviceWorker.register();
    }
    if(window.cordova) {
        document.addEventListener('deviceready', startApp, false)
    } else {
        startApp()
    }
    ```
9. Config public/index.html
    ```
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8" />
        <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta
          http-equiv="Content-Security-Policy"
          content="default-src 'self' data: gap: https://ssl.gstatic.com 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; media-src *; img-src 'self' data: content:;"
        />
        <meta name="format-detection" content="telephone=no"/>
        <meta name="msapplication-tap-highlight" content="no"/>
        <!--
          manifest.json provides metadata used when your web app is installed on a
          user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
        -->
        <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
        <!--
          Notice the use of %PUBLIC_URL% in the tags above.
          It will be replaced with the URL of the `public` folder during the build.
          Only files inside the `public` folder can be referenced from the HTML.
    
          Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
          work correctly both with client-side routing and a non-root public URL.
          Learn how to configure a non-root public URL by running `npm run build`.
        -->
        <title>React App</title>
      </head>
      <body>
        <noscript>You need to enable JavaScript to run this app.</noscript>
        <div id="root"></div>
        <!--
          This HTML file is a template.
          If you open it directly in the browser, you will see an empty page.
    
          You can add webfonts, meta tags, or analytics to this file.
          The build step will place the bundled scripts into the <body> tag.
    
          To begin the development, run `npm start` or `yarn start`.
          To create a production bundle, use `npm run build` or `yarn build`.
        -->
        <script type='text/javascript' src='cordova.js'></script>
      </body>
    </html>
    ```
10. 